using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnagramNameSpace;

namespace AnagramTests
{
    [TestClass]
    public class ReverseStringTests
    {
        [TestMethod]
        public void GetTheAnagramFullString()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("te1st as1d2(f", anagram.GetTheAnagram("ts1et fd1s2(a"));
        }

        [TestMethod]
        public void GetTheAnagramEmptyString()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("", anagram.GetTheAnagram(""));
        }

        [TestMethod]
        public void GetTheAnagramOnlyNotAlphabeticalCharscters()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("23_9!2&287367)))))))) 98239487^%#!&^", anagram.GetTheAnagram("23_9!2&287367)))))))) 98239487^%#!&^"));
        }

        [TestMethod]
        public void GetTheAnagramOnlyAlphabeticalCharacters()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("uecb uwqcl", anagram.GetTheAnagram("bceu lcqwu"));
        }

        [TestMethod]
        public void GetTheAnagramOnlySpaces()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("     ", anagram.GetTheAnagram("     "));
        }

        [TestMethod]
        public void GetTheAnagramMoreThanOneSpace()
        {
            Anagram anagram = new Anagram();

            Assert.AreEqual("df@@er!!    11cuenhcs#%j!", anagram.GetTheAnagram("re@@fd!!    11jschneu#%c!"));
        }
    }
}
