﻿using System;
using System.Linq;

namespace AnagramNameSpace
{
    public class Anagram
    {
        private string BaseString { get; set; }
        private AnagramSubstring[] SubStringsData { get; set; }
        private string[] SubStrings { get; set; }

        public string GetTheAnagram(string newBaseString)
        {
            BaseString = newBaseString;
            string[] substringsArray;

            if (!String.IsNullOrEmpty(BaseString))
            {
                SubStrings = BaseString.Split(" ");
                SubStringsData = new AnagramSubstring[SubStrings.Length];
                substringsArray = new string[SubStrings.Length];

                for (int i = 0; i < SubStrings.Length; i++)
                {
                    SubStringsData[i] = new AnagramSubstring(SubStrings[i]);
                    substringsArray[i] = SubStringsData[i].ReversedString;
                }

                return string.Join(' ', substringsArray);
            }
            else
            {
                return BaseString;
            }
        }
    }
}
