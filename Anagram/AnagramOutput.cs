﻿using System;

namespace AnagramNameSpace
{
    class AnagramOutput
    {
        private Anagram AnagramToOutput { get; set; }
        private string ReversedString { get; set; }
        private string BaseString { get; set; }

        public AnagramOutput(string baseString)
        {
            AnagramToOutput = new Anagram();
            BaseString = baseString;
            ReversedString = AnagramToOutput.GetTheAnagram(baseString);
        }

        public void OutputBaseString()
        {
            Console.WriteLine("Base string is: " + BaseString);
        }

        public void OutputAnagram()
        {
            Console.WriteLine("Anagram is: " + ReversedString);
        }
    }
}
