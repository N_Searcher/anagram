﻿using System;

namespace AnagramNameSpace
{
    class Program
    {
        static void Main(string[] args)
        {
            AnagramPrinting();
        }

        static string GetUsersInput()
        {
            ConsoleKeyInfo firstKeyPressed = Console.ReadKey(false);
            
            if (!(firstKeyPressed.Key == ConsoleKey.Escape))
            {
                return firstKeyPressed.KeyChar.ToString() + Console.ReadLine();
            }
            else
            {
                return null;
            }
        }

        static void AnagramPrinting()
        {
            while (true)
            {
                Console.Write("Insert string(or press Esc to exit): ");
                string userString = GetUsersInput();

                if (userString == null)
                {
                    Environment.Exit(0);
                    break;
                }

                AnagramOutput anagramOutput = new AnagramOutput(userString);

                anagramOutput.OutputBaseString();
                anagramOutput.OutputAnagram();
            }
        }
    }
}
