﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace AnagramNameSpace
{
    class AnagramSubstring
    {
        private string OriginalSubstring { get; set; }
        private string CleanString { get; set; }
        private MatchCollection NotAlphabeticCharacters { get; set; }

        public string ReversedString { get; private set; }

        public AnagramSubstring(string originalString)
        {
            OriginalSubstring = originalString;
            MakeSubstringData();
            ReverseString();
            PutBackNotAlphabeticCharacters();
        }

        private void MakeSubstringData()
        {
            Regex stringDataBuilder = new Regex(@"\W|\d|_");
            NotAlphabeticCharacters = stringDataBuilder.Matches(OriginalSubstring);
            CleanString = stringDataBuilder.Replace(OriginalSubstring, "");
        }

        private void PutBackNotAlphabeticCharacters()
        {
            foreach (Match item in NotAlphabeticCharacters)
            {
                ReversedString = ReversedString.Insert(item.Index, item.Value);
            }
        }

        private void ReverseString()
        {
            ReversedString = new string(CleanString.ToCharArray().Reverse().ToArray());
        }
    }
}
